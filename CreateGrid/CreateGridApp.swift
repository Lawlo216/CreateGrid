//
//  CreateGridApp.swift
//  CreateGrid
//
//  Created by Yin Yee Law on 1/5/2024.
//

import SwiftUI

@main
struct CreateGridApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
