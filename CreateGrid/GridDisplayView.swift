//
//  GridDisplayView.swift
//  CreateGrid
//
//  Created by Yin Yee Law on 1/5/2024.
//

import SwiftUI
import Combine

struct GridDisaplyView: View {
    var column: Int
    var row: Int
    
    @State private var randomColumn: Int? = 0
    @State private var randomRow: Int? = 0
    @State private var timer: Timer?
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        ZStack(alignment: .topLeading){
            Grid
            BackButton
        }
    }
    
    private func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { _ in
            generateRandomCoordinates()
        }
    }

    private func generateRandomCoordinates() {
        let newRandomColumn = Int.random(in: 0...column - 1)
        let newRandomRow = Int.random(in: 0...row - 1)

        DispatchQueue.main.async {
            randomColumn = newRandomColumn
            randomRow = newRandomRow
        }
    }

    private func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
}

extension GridDisaplyView {
    var Grid : some View {
        HStack(alignment: .center, spacing: 0) {
            ForEach(0..<column, id: \.self) { columnIndex in
                VStack(spacing: 0) {
                    ForEach(0..<row, id: \.self) { rowIndex in
                        Rectangle()
                            .foregroundColor(.gray)
                            .frame(maxWidth: .infinity, maxHeight: .infinity)
                            .overlay(
                                Text(columnIndex == randomColumn && rowIndex == randomRow ? "random" : "")
                                    .foregroundColor(.white)
                                    .fontWeight(.semibold)
                            )
                            .padding(2)
                            .offset(y: -2)
                    }
                    .background(Color.white)
                    
                    Button(action: {
                        if(columnIndex == randomColumn) {
                            randomColumn = nil
                            randomRow = nil
                        }
                    }) {
                        Text("Check")
                            .foregroundColor(.white)
                            .frame(height: 50)
                            .frame(maxWidth: .infinity)
                            .foregroundColor(.primary)
                            .background(columnIndex == randomColumn ? Color.yellow : Color.blue)
                            .padding(2)
                    }
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .onAppear {
            startTimer()
        }
        .onDisappear {
            stopTimer()
        }
    }
}

extension GridDisaplyView {
    var BackButton: some View {
        Button {
            dismiss()
        } label: {
            HStack {
                Image(systemName: "chevron.backward")
                Text("Back")
                    .foregroundColor(.primary)
            }
            
        }
        .padding(.vertical, 40)
        .padding(.horizontal, 15)
        .foregroundColor(.primary)
    }
}


#Preview {
    GridDisaplyView(column: 2, row: 2)
}
