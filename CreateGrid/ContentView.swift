//
//  ContentView.swift
//  CreateGrid
//
//  Created by Yin Yee Law on 1/5/2024.
//

import SwiftUI

struct ContentView: View {
    @State private var column: Int = 1
    @State private var row:  Int = 1
    @State private var showAlert: Bool = false
    @State private var showGrid: Bool = false
    
    private var maxColumn: Int {
        UIDevice.current.userInterfaceIdiom == .pad ? 10 : 5
    }
    private var maxRow: Int {
        UIDevice.current.userInterfaceIdiom == .pad ? 30 : 8
    }
    
    var body: some View {
        NavigationView {
            VStack {
                Text("Generate a Grid")
                    .font(.system(size: 50, weight: .medium, design: .serif))
                    .foregroundColor(Color(.black))
                    .padding(.vertical, 10)
                    
                HStack {
                    VStack(alignment: .leading){
                            Text("Rules:")
                            Text(" 1 <= column <= \(maxColumn)")
                            Text(" 1 <= row <= \(maxRow)")
                        }
                        .foregroundColor(.gray)
                        .padding(.horizontal, 10)
                    
                    Spacer()
                }
                .padding(.bottom, 40)
                
                VStack {
                    CustomTextField(name: "Column", value: $column)
                    CustomTextField(name: "Row", value: $row)
                    
                    CreateGridButton(action: checkGrid)
                }
                .background(
                    NavigationLink(
                       destination:
                        GridDisaplyView(column: column, row: row)
                            .ignoresSafeArea(.all)
                            .navigationBarBackButtonHidden(),
                       isActive: $showGrid,
                       label: { EmptyView() }
                   )
                )
                .modifier(AlertModifier(isPresented: $showAlert, maxColumn: maxColumn, maxRow: maxRow))
            }
            .frame(maxWidth: UIDevice.current.userInterfaceIdiom == .pad ? 500 : .infinity)
            .padding()
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
    
    func checkGrid() {
        if column > maxColumn  || row > maxRow || column <= 0 || row <= 0 {
            showAlert = true
            return
        }
        
        if !showAlert {
              showGrid = true
          }
    }
    
    func CustomTextField(name: String, value: Binding<Int>) -> some View {
        HStack {
            Text(name)
                .frame(width: 80)
                .fontWeight(.semibold)
            TextField("", value: value, formatter: NumberFormatter())
                .padding(12)
                .background(Color(.systemGray6))
                .cornerRadius(8)
                .keyboardType(.numberPad)
        }
        .font(.subheadline)
    }

}

struct CreateGridButton: View {
    var action: () -> Void
    
    var body: some View {
        Button(action: action) {
            Text("Submit")
                .font(.subheadline)
                .fontWeight(.semibold)
                .foregroundColor(.white)
                .padding(12)
                .frame(maxWidth: .infinity)
                .background(.black)
                .frame(height: 44)
                .cornerRadius(8)
        }
    }
}

struct AlertModifier: ViewModifier {
    @Binding var isPresented: Bool
    let maxColumn: Int
    let maxRow: Int
    
    func body(content: Content) -> some View {
        content.alert(isPresented: $isPresented) {
            Alert(
                title: Text("Please make sure the input match the grid requirement"),
                message: Text("1 <= column =< \(maxColumn)\n1 <= row =< \(maxRow)")
            )
        }
    }
}



#Preview {
    ContentView()
}
